// ------------------------------------------------------------------------------------------------------------
// File Name     	: operifft.v
// Created By     	: Gaurav
// Date          	: 05/09/17
// Description   	: This module implments the basic butterfly network of Inverse Fast Fourier Transformation.
// ------------------------------------------------------------------------------------------------------------

module operifft(clk,rst,inar,inai,inbr,inbi,twiddle_r,twiddle_i,outar,outai,outbr,outbi,pushin,pushout);

input clk,rst;
input [35:0] inar,inbr;
input [35:0] inai,inbi;
input [31:0] twiddle_r,twiddle_i;
output [35:0] outar,outai;
output [35:0] outbr,outbi;
input pushin;
output pushout;

wire [67:0] multr_part1,multr_part2;
wire [67:0] multi_part1,multi_part2;
wire [35:0] sumr,sumi;
wire [35:0] subr,subi;
reg [35:0] outar_d,outai_d;
reg pushout_0,pushout_1,pushout_2;
assign pushout= pushout_0;
always @(posedge(clk)) begin
	if(rst) begin
		pushout_0 <=0;
		pushout_1 <=0;
		pushout_2 <=0;
	end else begin
	pushout_0 <= pushin;
	pushout_1 <= pushout_0;
	pushout_2 <= pushout_1;
end
end


//multiply twiddle factor (real part)
DW02_mult_2_stage #(36,32) multr1(subr,twiddle_r,1'b1,clk,multr_part1);
//imagenery part(in real part) 
DW02_mult_2_stage #(36,32) multr2(subi,twiddle_i,1'b1,clk,multr_part2);

//real part after twidle factor calculation 
//fadd addr(clk,rst,multr_part1[51:24],{(multr_part2[51]+1'b1),multr_part2[50:24]},sumr_t);


//imaginary
//part 1
DW02_mult_2_stage #(36,32) multi1(subi,twiddle_r,1'b1,clk,multi_part1);
//part 2
DW02_mult_2_stage #(36,32) multi2(subr,twiddle_i,1'b1,clk,multi_part2);
//imaginary part of eq

DW01_addsub #(36) i0(.A(inar),.B(inbr),.CI(1'b0),.ADD_SUB(1'b0),.SUM(sumr),.CO());
DW01_addsub #(36) i1(.A(inai),.B(inbi),.CI(1'b0),.ADD_SUB(1'b0),.SUM(sumi),.CO());
DW01_addsub #(36) i2(.A(inar),.B(inbr),.CI(1'b0),.ADD_SUB(1'b1),.SUM(subr),.CO());
DW01_addsub #(36) i3(.A(inai),.B(inbi),.CI(1'b0),.ADD_SUB(1'b1),.SUM(subi),.CO());



//using lib adder
DW01_addsub #(36) i4(.A(multr_part1[63:28]),.B(multr_part2[63:28]),.CI(1'b0),.ADD_SUB(1'b1),.SUM(outbr),.CO());
DW01_addsub #(36) i5(.A(multi_part1[63:28]),.B(multi_part2[63:28]),.CI(1'b0),.ADD_SUB(1'b0),.SUM(outbi),.CO());

always @(posedge (clk) or posedge(rst)) begin
	if(rst) begin
		outar_d<=0;
		outai_d<=0;
	end else begin 
		outar_d<= sumr;
		outai_d<= sumi;
	end
end
assign outar = /*(inar[27]===1 && inbr[27]===1)?{1'b1,sumr[26:0]}:*/outar_d;
assign outai = /*(inai[27]===1 && inbi[27]===1)?{1'b1,sumi[26:0]}:*/outai_d; 
endmodule
