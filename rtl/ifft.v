// -------------------------------------------------------------------------------------------
// File Name     	: ifft.v
// Created By     	: Gaurav
// Date          	: 05/09/17
// Description   	: This module implements the 32 Point Inverse Fast Fourier Transformation 
// -------------------------------------------------------------------------------------------

module ifft(clk,rst,pushin,dir,dii,pushout,dor,doi);
input clk;
input rst;
input pushin;
input [27:0] dir;
input [27:0] dii;
output pushout;
output [27:0]dor;
output [27:0]doi;

parameter  S1 = 1;
parameter  S2 = 2;
parameter  S3 = 3;
parameter  S4 = 4;
parameter  S5 = 5;
parameter  S6 = 6;

reg [31:0] twiddle_mem_real[0:15];
reg [31:0] twiddle_mem_imag[0:15];
reg [35:0] mem_s1_r1[0:16];
reg [35:0] mem_s1_i1[0:16];
reg [35:0] mem_s1_r2[0:16];
reg [35:0] mem_s1_i2[0:16];
reg [35:0] mem_s2_r1[0:7];
reg [35:0] mem_s2_i1[0:7];
reg [35:0] mem_s2_r2[0:7];
reg [35:0] mem_s2_i2[0:7];
reg [35:0] mem_s2_r3[0:7];
reg [35:0] mem_s2_i3[0:7];
reg [35:0] mem_s2_r4[0:7];
reg [35:0] mem_s2_i4[0:7];
reg [35:0] mem_s3_r1[0:3];
reg [35:0] mem_s3_i1[0:3];
reg [35:0] mem_s3_r2[0:3];
reg [35:0] mem_s3_i2[0:3];
reg [35:0] mem_s3_r3[0:3];
reg [35:0] mem_s3_i3[0:3];
reg [35:0] mem_s3_r4[0:3];
reg [35:0] mem_s3_i4[0:3];
reg [35:0] mem_s3_r5[0:3];
reg [35:0] mem_s3_i5[0:3];
reg [35:0] mem_s3_r6[0:3];
reg [35:0] mem_s3_i6[0:3];
reg [35:0] mem_s3_r7[0:3];
reg [35:0] mem_s3_i7[0:3];
reg [35:0] mem_s3_r8[0:3];
reg [35:0] mem_s3_i8[0:3];
reg [35:0] mem_s4_r1[0:1];
reg [35:0] mem_s4_i1[0:1];
reg [35:0] mem_s4_r2[0:1];
reg [35:0] mem_s4_i2[0:1];
reg [35:0] mem_s4_r3[0:1];
reg [35:0] mem_s4_i3[0:1];
reg [35:0] mem_s4_r4[0:1];
reg [35:0] mem_s4_i4[0:1];
reg [35:0] mem_s4_r5[0:1];
reg [35:0] mem_s4_i5[0:1];
reg [35:0] mem_s4_r6[0:1];
reg [35:0] mem_s4_i6[0:1];
reg [35:0] mem_s4_r7[0:1];
reg [35:0] mem_s4_i7[0:1];
reg [35:0] mem_s4_r8[0:1];
reg [35:0] mem_s4_i8[0:1];
reg [35:0] mem_s4_r9[0:1];
reg [35:0] mem_s4_i9[0:1];
reg [35:0] mem_s4_r10[0:1];
reg [35:0] mem_s4_i10[0:1];
reg [35:0] mem_s4_r11[0:1];
reg [35:0] mem_s4_i11[0:1];
reg [35:0] mem_s4_r12[0:1];
reg [35:0] mem_s4_i12[0:1];
reg [35:0] mem_s4_r13[0:1];
reg [35:0] mem_s4_i13[0:1];
reg [35:0] mem_s4_r14[0:1];
reg [35:0] mem_s4_i14[0:1];
reg [35:0] mem_s4_r15[0:1];
reg [35:0] mem_s4_i15[0:1];
reg [35:0] mem_s4_r16[0:1];
reg [35:0] mem_s4_i16[0:1];
reg [35:0] mem_s5_r1[0:16];
reg [35:0] mem_s5_i1[0:16];
reg [35:0] mem_s5_r2[0:16];
reg [35:0] mem_s5_i2[0:16];
reg [35:0] mem_res_r[0:31];
reg [35:0] mem_res_i[0:31];
reg empty;

reg [4:0] write_s1_c;
reg [3:0] write_s2_c;
reg [3:0] write_s3_c;
reg [3:0] write_s4_c;
reg [3:0] write_s5_c;
reg [3:0] write_res_c;
reg [3:0] read_s1_c;
reg [3:0] read_s2_c;
reg [3:0] read_s3_c;
reg [3:0] read_s4_c;
reg [3:0] read_s5_c;
reg [4:0] read_res_c;
reg [35:0] inar,inbr,inai,inbi;
reg pushin_f,pushout_d;
wire pushout_f,pushout_d_1;
reg [31:0] twiddle_r,twiddle_i;
reg [27:0] dor_d,doi_d;
reg [35:0] resar,resai,resbr,resbi;

/*AUTOWIRE*/
// Beginning of automatic wires (for undeclared instantiated-module outputs)
wire [35:0]		outai;			// From subi0 of operifft.v
wire [35:0]		outar;			// From subi0 of operifft.v
wire [35:0]		outbi;			// From subi0 of operifft.v
wire [35:0]		outbr;			// From subi0 of operifft.v
// End of automatics

reg [5:0] state_con;

always @(posedge (clk)) begin
    twiddle_mem_real[0] = 32'h10000000;
    twiddle_mem_imag[0] = 32'h00000000;
    
    twiddle_mem_real[1] = 32'h0FB14BE7;
    twiddle_mem_imag[1] = 32'h031F1707;
    
    twiddle_mem_real[2] = 32'h0EC835E7;
    twiddle_mem_imag[2] = 32'h061F78A9;
    
    twiddle_mem_real[3] = 32'h0D4DB314;
    twiddle_mem_imag[3] = 32'h08E39D9C;
    
    twiddle_mem_real[4] = 32'h0B504F33;
    twiddle_mem_imag[4] = 32'h0B504F33;
    
    twiddle_mem_real[5] = 32'h08E39D9C;
    twiddle_mem_imag[5] = 32'h0D4DB314;

    twiddle_mem_real[6] = 32'h061F78A9;
    twiddle_mem_imag[6] = 32'h0EC835E7;

    twiddle_mem_real[7] = 32'h031F1707;
    twiddle_mem_imag[7] = 32'h0FB14BE7;

    twiddle_mem_real[8] = 32'h00000000;
    twiddle_mem_imag[8] = 32'h10000000;

    twiddle_mem_real[9] = 32'hFCE0E8F8;
    twiddle_mem_imag[9] = 32'h0FB14BE7;

    twiddle_mem_real[10] = 32'hF9E08756;
    twiddle_mem_imag[10] = 32'h0EC835E7;
    
    twiddle_mem_real[11] = 32'hF71C6263;
    twiddle_mem_imag[11] = 32'h0D4DB314;
    
    twiddle_mem_real[12] = 32'hF4AFB0CC;
    twiddle_mem_imag[12] = 32'h0B504F33;
    
    twiddle_mem_real[13] = 32'hF2B24CEB;
    twiddle_mem_imag[13] = 32'h08E39D9C;
    
    twiddle_mem_real[14] = 32'hF137CA18;
    twiddle_mem_imag[14] = 32'h061F78A9;

    twiddle_mem_real[15] = 32'hF04EB418;
    twiddle_mem_imag[15] = 32'h031F1707;
    
end

always @(posedge(clk) or posedge(rst)) begin
	if (rst)begin
		write_s1_c <= 0;
	end else begin
	       	if (pushin == 1 && write_s1_c < 16) begin
		mem_s1_r1[write_s1_c] <= {{8{dir[27]}},dir};
	        mem_s1_i1[write_s1_c] <= {{8{dii[27]}},dii};
	end else if(pushin ==1) begin
		mem_s1_r2[write_s1_c-16] <= {{8{dir[27]}},dir};
	        mem_s1_i2[write_s1_c-16] <= {{8{dii[27]}},dii};
	end

	write_s1_c <= (pushin)?write_s1_c+1:write_s1_c;
	end	
end

//full and empty control s1
always @(*) begin
	if(rst) begin 
		empty <=1;
	end else begin 
		empty <= (((write_s1_c > 16) ||((write_s1_c==0) && (read_s1_c== 'd15))) && ((write_s1_c-16) != read_s1_c)) ? 1'b0:1'b1;
	end
end	

//State control 
always @(posedge (clk) or posedge(rst)) begin
if(rst) begin
	state_con <= S1;
	write_s2_c <= 0;
	write_s3_c <= 0;
	write_s4_c <= 0;
	write_s5_c <= 0;
	write_res_c <=0;
	read_s1_c <= 0;
	read_s2_c <= 0;
	read_s3_c <= 0;
	read_s4_c <= 0;
	read_s5_c <= 0;
	read_res_c <= 0;
	pushin_f <= 0;
	inar <= 0;
	inai <= 0;
	inbr <= 0;
	inbi <= 0;
	dor_d <= 0;
	doi_d <= 0;
end else begin
	case(state_con) 
		S1: begin
			if ((!empty) && ((read_s1_c < (write_s1_c-16)) || ((write_s1_c==0) && (read_s1_c== 'd15)))) begin
				inar <= mem_s1_r1[read_s1_c];
				inai <= mem_s1_i1[read_s1_c];
				inbr <= mem_s1_r2[read_s1_c];
				inbi <= mem_s1_i2[read_s1_c];
				twiddle_r <= twiddle_mem_real[read_s1_c];
				twiddle_i <= twiddle_mem_imag[read_s1_c];
				pushin_f <=1;
				read_s1_c <= read_s1_c+1;
			end else begin
				pushin_f<=0;
			end
			if(pushout_f==1 && write_s2_c < 8) begin
				mem_s2_r1[write_s2_c] <= outar;
	       			mem_s2_i1[write_s2_c] <= outai;
				mem_s2_r3[write_s2_c] <= outbr;
	       			mem_s2_i3[write_s2_c] <= outbi;
			end else if(pushout_f==1 && (write_s2_c >= 8)) begin
				mem_s2_r2[write_s2_c-8] <= outar;
	       			mem_s2_i2[write_s2_c-8] <= outai;
				mem_s2_r4[write_s2_c-8] <= outbr;
	       			mem_s2_i4[write_s2_c-8] <= outbi;
 				state_con <= (write_s2_c ==15 )? S2: S1;
			end
			if(pushout_f) begin
				write_s2_c <= write_s2_c+1;
			end	
		end
		S2: begin
			if (read_s2_c <=15 && (read_s2_c >= write_s3_c)) begin
				inar <=(read_s2_c < 8)? mem_s2_r1[read_s2_c]:mem_s2_r3[read_s2_c-8];
				inai <=(read_s2_c < 8)? mem_s2_i1[read_s2_c]:mem_s2_i3[read_s2_c-8];
				inbr <=(read_s2_c < 8)? mem_s2_r2[read_s2_c]:mem_s2_r4[read_s2_c-8];
				inbi <=(read_s2_c < 8)? mem_s2_i2[read_s2_c]:mem_s2_i4[read_s2_c-8];
				twiddle_r <=(read_s2_c < 8)? twiddle_mem_real[2*read_s2_c]:twiddle_mem_real[2*(read_s2_c-8)];
				twiddle_i <=(read_s2_c < 8)? twiddle_mem_imag[2*read_s2_c]:twiddle_mem_imag[2*(read_s2_c-8)];
				read_s2_c <= read_s2_c+1;
				pushin_f <=1;
			end else begin
				pushin_f<=0;
			end
			if(pushout_f==1 && write_s3_c < 4) begin
				mem_s3_r1[write_s3_c] <= outar;
	       			mem_s3_i1[write_s3_c] <= outai;
				mem_s3_r3[write_s3_c] <= outbr;
	       			mem_s3_i3[write_s3_c] <= outbi;
			end else if(pushout_f==1 && (write_s3_c >= 4 && write_s3_c < 8)) begin
				mem_s3_r2[write_s3_c-4] <= outar;
	       			mem_s3_i2[write_s3_c-4] <= outai;
				mem_s3_r4[write_s3_c-4] <= outbr;
	       			mem_s3_i4[write_s3_c-4] <= outbi;
			end else if(pushout_f==1 && (write_s3_c >= 8 && write_s3_c < 12)) begin
				mem_s3_r5[write_s3_c-8] <= outar;
	       			mem_s3_i5[write_s3_c-8] <= outai;
				mem_s3_r7[write_s3_c-8] <= outbr;
	       			mem_s3_i7[write_s3_c-8] <= outbi;
			end else if(pushout_f==1 && (write_s3_c >= 12)) begin
				mem_s3_r6[write_s3_c-12] <= outar;
	       			mem_s3_i6[write_s3_c-12] <= outai;
				mem_s3_r8[write_s3_c-12] <= outbr;
	       			mem_s3_i8[write_s3_c-12] <= outbi;
 				state_con <= (write_s3_c ==15 )? S3: S2;
			end
			if(pushout_f) begin
				write_s3_c <= write_s3_c+1;
			end	
		end
		S3: begin
			if (read_s3_c <=15 && (read_s3_c >= write_s4_c)) begin
				inar <=(read_s3_c < 4)? mem_s3_r1[read_s3_c]: (read_s3_c < 8)?mem_s3_r3[read_s3_c-4]:(read_s3_c < 12)?mem_s3_r5[read_s3_c-8]:mem_s3_r7[read_s3_c-12];
				inai <=(read_s3_c < 4)? mem_s3_i1[read_s3_c]: (read_s3_c < 8)?mem_s3_i3[read_s3_c-4]:(read_s3_c < 12)?mem_s3_i5[read_s3_c-8]:mem_s3_i7[read_s3_c-12];
				inbr <=(read_s3_c < 4)? mem_s3_r2[read_s3_c]: (read_s3_c < 8)?mem_s3_r4[read_s3_c-4]:(read_s3_c < 12)?mem_s3_r6[read_s3_c-8]:mem_s3_r8[read_s3_c-12];
				inbi <=(read_s3_c < 4)? mem_s3_i2[read_s3_c]: (read_s3_c < 8)?mem_s3_i4[read_s3_c-4]:(read_s3_c < 12)?mem_s3_i6[read_s3_c-8]:mem_s3_i8[read_s3_c-12];
				twiddle_r <=(read_s3_c < 4)? twiddle_mem_real[4*read_s3_c]:(read_s3_c < 8)?twiddle_mem_real[4*(read_s3_c-4)]:(read_s3_c < 12)?twiddle_mem_real[4*(read_s3_c-8)]:twiddle_mem_real[4*(read_s3_c-12)];
				twiddle_i <=(read_s3_c < 4)? twiddle_mem_imag[4*read_s3_c]:(read_s3_c < 8)?twiddle_mem_imag[4*(read_s3_c-4)]:(read_s3_c < 12)?twiddle_mem_imag[4*(read_s3_c-8)]:twiddle_mem_imag[4*(read_s3_c-12)];
				read_s3_c <= read_s3_c+1;
				pushin_f <=1;
			end else begin
				pushin_f<=0;
			end
			if(pushout_f==1 && write_s4_c < 2) begin
				mem_s4_r1[write_s4_c] <= outar;
	       			mem_s4_i1[write_s4_c] <= outai;
				mem_s4_r3[write_s4_c] <= outbr;
	       			mem_s4_i3[write_s4_c] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 2 && write_s4_c < 4)) begin
				mem_s4_r2[write_s4_c-2] <= outar;
	       			mem_s4_i2[write_s4_c-2] <= outai;
				mem_s4_r4[write_s4_c-2] <= outbr;
	       			mem_s4_i4[write_s4_c-2] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 4 && write_s4_c < 6)) begin
				mem_s4_r5[write_s4_c-4] <= outar;
	       			mem_s4_i5[write_s4_c-4] <= outai;
				mem_s4_r7[write_s4_c-4] <= outbr;
	       			mem_s4_i7[write_s4_c-4] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 6 && write_s4_c < 8)) begin
				mem_s4_r6[write_s4_c-6] <= outar;
	       			mem_s4_i6[write_s4_c-6] <= outai;
				mem_s4_r8[write_s4_c-6] <= outbr;
	       			mem_s4_i8[write_s4_c-6] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 8 && write_s4_c < 10)) begin
				mem_s4_r9[write_s4_c-8] <= outar;
	       			mem_s4_i9[write_s4_c-8] <= outai;
				mem_s4_r11[write_s4_c-8] <= outbr;
	       			mem_s4_i11[write_s4_c-8] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 10 && write_s4_c < 12)) begin
				mem_s4_r10[write_s4_c-10] <= outar;
	       			mem_s4_i10[write_s4_c-10] <= outai;
				mem_s4_r12[write_s4_c-10] <= outbr;
	       			mem_s4_i12[write_s4_c-10] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 12 && write_s4_c < 14)) begin
				mem_s4_r13[write_s4_c-12] <= outar;
	       			mem_s4_i13[write_s4_c-12] <= outai;
				mem_s4_r15[write_s4_c-12] <= outbr;
	       			mem_s4_i15[write_s4_c-12] <= outbi;
			end else if(pushout_f==1 && (write_s4_c >= 14)) begin
				mem_s4_r14[write_s4_c-14] <= outar;
	       			mem_s4_i14[write_s4_c-14] <= outai;
				mem_s4_r16[write_s4_c-14] <= outbr;
	       			mem_s4_i16[write_s4_c-14] <= outbi;
 				state_con <= (write_s4_c ==15 )? S4: S3;
			end
			if(pushout_f) begin
				write_s4_c <= write_s4_c+1;
			end	
		end
		S4: begin
			if (read_s4_c <=15 && (read_s4_c >= write_s5_c)) begin
				inar <=(read_s4_c < 2)? mem_s4_r1[read_s4_c]: (read_s4_c < 4)?mem_s4_r3[read_s4_c-2]:(read_s4_c < 6)?mem_s4_r5[read_s4_c-4]:(read_s4_c < 8)?mem_s4_r7[read_s4_c-6]:(read_s4_c < 10)?mem_s4_r9[read_s4_c-8]:(read_s4_c < 12)?mem_s4_r11[read_s4_c-10]:(read_s4_c < 14)?mem_s4_r13[read_s4_c-12]:mem_s4_r15[read_s4_c-14];
				inai <=(read_s4_c < 2)? mem_s4_i1[read_s4_c]: (read_s4_c < 4)?mem_s4_i3[read_s4_c-2]:(read_s4_c < 6)?mem_s4_i5[read_s4_c-4]:(read_s4_c < 8)?mem_s4_i7[read_s4_c-6]:(read_s4_c < 10)?mem_s4_i9[read_s4_c-8]:(read_s4_c < 12)?mem_s4_i11[read_s4_c-10]:(read_s4_c < 14)?mem_s4_i13[read_s4_c-12]:mem_s4_i15[read_s4_c-14];
				inbr <=(read_s4_c < 2)? mem_s4_r2[read_s4_c]: (read_s4_c < 4)?mem_s4_r4[read_s4_c-2]:(read_s4_c < 6)?mem_s4_r6[read_s4_c-4]:(read_s4_c < 8)?mem_s4_r8[read_s4_c-6]:(read_s4_c < 10)?mem_s4_r10[read_s4_c-8]:(read_s4_c < 12)?mem_s4_r12[read_s4_c-10]:(read_s4_c < 14)?mem_s4_r14[read_s4_c-12]:mem_s4_r16[read_s4_c-14];
				inbi <=(read_s4_c < 2)? mem_s4_i2[read_s4_c]: (read_s4_c < 4)?mem_s4_i4[read_s4_c-2]:(read_s4_c < 6)?mem_s4_i6[read_s4_c-4]:(read_s4_c < 8)?mem_s4_i8[read_s4_c-6]:(read_s4_c < 10)?mem_s4_i10[read_s4_c-8]:(read_s4_c < 12)?mem_s4_i12[read_s4_c-10]:(read_s4_c < 14)?mem_s4_i14[read_s4_c-12]:mem_s4_i16[read_s4_c-14];
				twiddle_r <=(read_s4_c < 2)? twiddle_mem_real[8*read_s4_c]: (read_s4_c < 4)? twiddle_mem_real[8*(read_s4_c-2)]:(read_s4_c < 6)? twiddle_mem_real[8*(read_s4_c-4)]:(read_s4_c < 8)? twiddle_mem_real[8*(read_s4_c-6)]:(read_s4_c < 10)? twiddle_mem_real[8*(read_s4_c-8)]:(read_s4_c < 12)? twiddle_mem_real[8*(read_s4_c-10)]:(read_s4_c < 12)? twiddle_mem_real[8*(read_s4_c-10)]:(read_s4_c < 14)? twiddle_mem_real[8*(read_s4_c-12)]:twiddle_mem_real[8*(read_s4_c-14)];
				
				twiddle_i <=(read_s4_c < 2)? twiddle_mem_imag[8*read_s4_c]: (read_s4_c < 4)? twiddle_mem_imag[8*(read_s4_c-2)]:(read_s4_c < 6)? twiddle_mem_imag[8*(read_s4_c-4)]:(read_s4_c < 8)? twiddle_mem_imag[8*(read_s4_c-6)]:(read_s4_c < 10)? twiddle_mem_imag[8*(read_s4_c-8)]:(read_s4_c < 12)? twiddle_mem_imag[8*(read_s4_c-10)]:(read_s4_c < 12)? twiddle_mem_imag[8*(read_s4_c-10)]:(read_s4_c < 14)? twiddle_mem_imag[8*(read_s4_c-12)]:twiddle_mem_imag[8*(read_s4_c-14)];
				read_s4_c <= read_s4_c+1;
				pushin_f <=1;
			end else begin
				pushin_f<=0;
			end
			if(pushout_f==1 && (write_s5_c == 0) || (write_s5_c == 2) || (write_s5_c == 4) || (write_s5_c == 6) || (write_s5_c == 8) || (write_s5_c == 10) || (write_s5_c == 12) || (write_s5_c == 14)) begin
				mem_s5_r1[write_s5_c] <= outar;
	       			mem_s5_i1[write_s5_c] <= outai;
				mem_s5_r1[write_s5_c+1] <= outbr;
	       			mem_s5_i1[write_s5_c+1] <= outbi;
			end   else if(pushout_f ) begin
				mem_s5_r2[write_s5_c-1] <= outar;
	       			mem_s5_i2[write_s5_c-1] <= outai;
				mem_s5_r2[write_s5_c] <= outbr;
	       			mem_s5_i2[write_s5_c] <= outbi;
 				state_con <= (write_s5_c ==15 )? S5: S4;
			end
			if(pushout_f) begin
				write_s5_c <= write_s5_c+1;
			end	
		end
		S5: begin
				if (read_s5_c <=15 && (read_s5_c >= write_res_c)) begin
					inar <= mem_s5_r1[read_s5_c];
					inai <= mem_s5_i1[read_s5_c];
					inbr <= mem_s5_r2[read_s5_c];
					inbi <= mem_s5_i2[read_s5_c];
					twiddle_r <= twiddle_mem_real[0];
					twiddle_i <= twiddle_mem_imag[0];
					pushin_f <=1;
					read_s5_c <= read_s5_c+1;
				end else begin
					pushin_f<=0;
				end
				if(pushout_f ) begin
					mem_res_r[0] <= (write_res_c == 0 )? resar : mem_res_r[0];
					mem_res_i[0] <= (write_res_c == 0 )? resai : mem_res_i[0];
					mem_res_r[16] <= (write_res_c == 0 )? resbr : mem_res_r[16];
					mem_res_i[16] <= (write_res_c == 0 )? resbi : mem_res_i[16];
					mem_res_r[8] <= (write_res_c == 1 )? resar : mem_res_r[8];
					mem_res_i[8] <= (write_res_c == 1 )? resai : mem_res_i[8];
					mem_res_r[24] <= (write_res_c == 1 )? resbr : mem_res_r[24];
					mem_res_i[24] <= (write_res_c == 1 )? resbi : mem_res_i[24];
					mem_res_r[4] <= (write_res_c == 2 )? resar : mem_res_r[4];
					mem_res_i[4] <= (write_res_c == 2 )? resai : mem_res_i[4];
					mem_res_r[20] <= (write_res_c == 2 )? resbr : mem_res_r[20];
					mem_res_i[20] <= (write_res_c == 2 )? resbi : mem_res_i[20];
					mem_res_r[12] <= (write_res_c == 3 )? resar : mem_res_r[12];
					mem_res_i[12] <= (write_res_c == 3 )? resai : mem_res_i[12];
					mem_res_r[28] <= (write_res_c == 3 )? resbr : mem_res_r[28];
					mem_res_i[28] <= (write_res_c == 3 )? resbi : mem_res_i[28];
					mem_res_r[2] <= (write_res_c == 4 )? resar : mem_res_r[2];
					mem_res_i[2] <= (write_res_c == 4 )? resai : mem_res_i[2];
					mem_res_r[18] <= (write_res_c == 4 )? resbr : mem_res_r[18];
					mem_res_i[18] <= (write_res_c == 4 )? resbi : mem_res_i[18];
					mem_res_r[10] <= (write_res_c == 5 )? resar : mem_res_r[10];
					mem_res_i[10] <= (write_res_c == 5 )? resai : mem_res_i[10];
					mem_res_r[26] <= (write_res_c == 5 )? resbr : mem_res_r[26];
					mem_res_i[26] <= (write_res_c == 5 )? resbi : mem_res_i[26];
					mem_res_r[6] <= (write_res_c == 6 )? resar : mem_res_r[6];
					mem_res_i[6] <= (write_res_c == 6 )? resai : mem_res_i[6];
					mem_res_r[22] <= (write_res_c == 6 )? resbr : mem_res_r[22];
					mem_res_i[22] <= (write_res_c == 6 )? resbi : mem_res_i[22];
					mem_res_r[14] <= (write_res_c == 7 )? resar : mem_res_r[14];
					mem_res_i[14] <= (write_res_c == 7 )? resai : mem_res_i[14];
					mem_res_r[30] <= (write_res_c == 7 )? resbr : mem_res_r[30];
					mem_res_i[30] <= (write_res_c == 7 )? resbi : mem_res_i[30];
					mem_res_r[1] <= (write_res_c == 8 )? resar : mem_res_r[1];
					mem_res_i[1] <= (write_res_c == 8 )? resai : mem_res_i[1];
					mem_res_r[17] <= (write_res_c == 8 )? resbr : mem_res_r[17];
					mem_res_i[17] <= (write_res_c == 8 )? resbi : mem_res_i[17];
					mem_res_r[9] <= (write_res_c == 9 )? resar : mem_res_r[9];
					mem_res_i[9] <= (write_res_c == 9 )? resai : mem_res_i[9];
					mem_res_r[25] <= (write_res_c == 9 )? resbr : mem_res_r[25];
					mem_res_i[25] <= (write_res_c == 9 )? resbi : mem_res_i[25];
					mem_res_r[5] <= (write_res_c == 10 )? resar : mem_res_r[5];
					mem_res_i[5] <= (write_res_c == 10 )? resai : mem_res_i[5];
					mem_res_r[21] <= (write_res_c == 10 )? resbr : mem_res_r[21];
					mem_res_i[21] <= (write_res_c == 10 )? resbi : mem_res_i[21];
					mem_res_r[13] <= (write_res_c == 11 )? resar : mem_res_r[13];
					mem_res_i[13] <= (write_res_c == 11 )? resai : mem_res_i[13];
					mem_res_r[29] <= (write_res_c == 11 )? resbr : mem_res_r[29];
					mem_res_i[29] <= (write_res_c == 11 )? resbi : mem_res_i[29];
					mem_res_r[3] <= (write_res_c == 12 )? resar : mem_res_r[3];
					mem_res_i[3] <= (write_res_c == 12 )? resai : mem_res_i[3];
					mem_res_r[19] <= (write_res_c == 12 )? resbr : mem_res_r[19];
					mem_res_i[19] <= (write_res_c == 12 )? resbi : mem_res_i[19];
					mem_res_r[11] <= (write_res_c == 13 )? resar : mem_res_r[11];
					mem_res_i[11] <= (write_res_c == 13 )? resai : mem_res_i[11];
					mem_res_r[27] <= (write_res_c == 13 )? resbr : mem_res_r[27];
					mem_res_i[27] <= (write_res_c == 13 )? resbi : mem_res_i[27];
					mem_res_r[7] <= (write_res_c == 14 )? resar : mem_res_r[7];
					mem_res_i[7] <= (write_res_c == 14 )? resai : mem_res_i[7];
					mem_res_r[23] <= (write_res_c == 14 )? resbr : mem_res_r[23];
					mem_res_i[23] <= (write_res_c == 14 )? resbi : mem_res_i[23];
					mem_res_r[15] <= (write_res_c == 15 )? resar : mem_res_r[15];
					mem_res_i[15] <= (write_res_c == 15 )? resai : mem_res_i[15];
					mem_res_r[31] <= (write_res_c == 15 )? resbr : mem_res_r[31];
					mem_res_i[31] <= (write_res_c == 15 )? resbi : mem_res_i[31];
 					state_con <= (write_res_c ==15 )? S6: S5;
					write_res_c <= write_res_c+1;
				end
			end
			S6: begin
				if(read_res_c <= 31) begin
					pushin_f <=0;
					dor_d <= #1  mem_res_r[read_res_c][27:0];
					doi_d <= #1 mem_res_i[read_res_c][27:0];
 					state_con <= (read_res_c =='d31 )? S1: S6;
					read_res_c <= read_res_c+1;

				end
			end
			default : state_con <=S1 ;
	endcase
end
end
assign dor = (&pushout_d_1)? dor_d :28'h0 ;
assign doi = (&pushout_d_1)?doi_d:28'h0 ;
assign pushout = &pushout_d_1;
assign pushout_d_1 = pushout_d; 
always @(posedge (clk) or posedge (rst)) begin
				if(rst)begin
					pushout_d<= #1 0;
				end else if(state_con == S6) begin
					pushout_d <= #1 1;
				end else begin
					pushout_d <= #1 0;
				end

end

always @(*) begin
if(rst)begin
	resai<=0;
	resar<=0;
	resbr<=0;
	resbi<=0;
end else begin
	resar <= (outar/8'h20);
	resai <= (outai/8'h20);
	resbr <= (outbr/8'h20);
	resbi <= (outbi/8'h20);
end
end
/*
DW_div #(36,8)di1(.a(outar), .b(8'h20), .quotient(resar), .remainder(), .divide_by_0());
DW_div #(36,8)di2(.a(outai), .b(8'h20), .quotient(resai), .remainder(), .divide_by_0());
DW_div #(36,8)di3(.a(outbr), .b(8'h20), .quotient(resbr), .remainder(), .divide_by_0());
DW_div #(36,8)di4(.a(outbi), .b(8'h20), .quotient(resbi), .remainder(), .divide_by_0());
*/
operifft subi0(
	      // Outputs
	      .outar			(outar[35:0]),
	      .outai			(outai[35:0]),
	      .outbr			(outbr[35:0]),
	      .outbi			(outbi[35:0]),
	      .pushout			(pushout_f),
	      // Inputs
	      .clk			(clk),
	      .rst			(rst),
	      .inar			(inar[35:0]),
	      .inbr			(inbr[35:0]),
	      .inai			(inai[35:0]),
	      .inbi			(inbi[35:0]),
	      .twiddle_r		(twiddle_r[31:0]),
	      .twiddle_i		(twiddle_i[31:0]),
	      .pushin			(pushin_f));


endmodule



