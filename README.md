#
## DESCRIPTION - DESIGN AND SYNTHESIS OF INVERSE FAST FOURIER TRANSFORMATION

This work aims at design, synthesis and gate level simulation of 32 point Inverse Fast Fourier Transformation.The design has been 
approached by the method of decimation in frequency (DIF) using the first-half/second-half technique. 
The DIF computation method have been illustrated below for only 8 point DIF – 

![1.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/22369589-34e6-4115-a436-60bdcdce78c1)

The entire design has been implemented as finite state machine (FSM) to meet constraint of minimum resource/gate utilization. 
Since the basic task in all 5 stage is the computation of butterfly entity(operifft.v) with different twiddle factors. 
Each stages in the FSM reuses the butterfly(operifft.v) module to compute the values and stores the result in the 2D array of 36 bits
at each stage. The stored values are again fed to the butterfly module in next stage with right pair of data and twiddle factors.
