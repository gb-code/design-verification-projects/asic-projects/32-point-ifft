module operifft(clk,rst,inar,inai,inbr,inbi,twiddle_r,twiddle_i,outar,outai,outbr,outbi,pushin,pushout);

input clk,rst;
input [35:0] inar,inbr;
input [35:0] inai,inbi;
input [31:0] twiddle_r,twiddle_i;
output [35:0] outar,outai;
output [35:0] outbr,outbi;
input pushin;
output pushout;

wire [67:0] multr_part1,multr_part2;
wire [67:0] multi_part1,multi_part2;
wire [35:0] sumr,sumi;
wire [35:0] subr,subi;
reg [35:0] outar_d,outai_d;
reg pushout_0,pushout_1,pushout_2;
assign pushout= pushout_0;
always @(posedge(clk)) begin
	if(rst) begin
		pushout_0 <=0;
		pushout_1 <=0;
		pushout_2 <=0;
	end else begin
	pushout_0 <= pushin;
	pushout_1 <= pushout_0;
	pushout_2 <= pushout_1;
end
end


//multiply twiddle factor (real part)
DW02_mult_2_stage #(36,32) multr1(subr,twiddle_r,1'b1,clk,multr_part1);
//imagenery part(in real part) 
DW02_mult_2_stage #(36,32) multr2(subi,twiddle_i,1'b1,clk,multr_part2);

//real part after twidle factor calculation 
//fadd addr(clk,rst,multr_part1[51:24],{(multr_part2[51]+1'b1),multr_part2[50:24]},sumr_t);


//imaginary
//part 1
DW02_mult_2_stage #(36,32) multi1(subi,twiddle_r,1'b1,clk,multi_part1);
//part 2
DW02_mult_2_stage #(36,32) multi2(subr,twiddle_i,1'b1,clk,multi_part2);
//imaginary part of eq
//fadd addi(clk,rst,multi_part1[51:24],multi_part2[51:24],sumi_t);


//sum calculation top output(real)
//fadd addoutr1(clk,rst,inar,sumr_t,outar);
//sum calculation top output(imaginary)
//fadd addouti1(clk,rst,inai,sumi_t,outai);

//Down output
//fadd addoutr2(clk,rst,inar,{(sumr_t[35]+1'b1),sumr_t[26:0]},outbr);
//fadd addouti2(clk,rst,inai,{(sumi_t[35]+1'b1),sumi_t[26:0]},outbi);

/*
assign outar =inar+({multr_part1[63],multr_part1[54:52],multr_part1[47:24]}-{multr_part2[63],multr_part2[54:52],multr_part2[47:24]});
assign outai =inai+({multi_part1[63],multi_part1[54:52],multi_part1[47:24]}+{multi_part2[63],multi_part2[54:52],multi_part2[47:24]});
assign outbr =inar-({multr_part1[63],multr_part1[54:52],multr_part1[47:24]}-{multr_part2[63],multr_part2[54:52],multr_part2[47:24]});
assign outbi =inar-({multi_part1[63],multi_part1[54:52],multi_part1[47:24]}+{multi_part2[63],multi_part2[54:52],multi_part2[47:24]});
*/

DW01_addsub #(36) i0(.A(inar),.B(inbr),.CI(1'b0),.ADD_SUB(1'b0),.SUM(sumr),.CO());
DW01_addsub #(36) i1(.A(inai),.B(inbi),.CI(1'b0),.ADD_SUB(1'b0),.SUM(sumi),.CO());
DW01_addsub #(36) i2(.A(inar),.B(inbr),.CI(1'b0),.ADD_SUB(1'b1),.SUM(subr),.CO());
DW01_addsub #(36) i3(.A(inai),.B(inbi),.CI(1'b0),.ADD_SUB(1'b1),.SUM(subi),.CO());


/*
always @(posedge clk) begin
sumr<= inar+inbr;
sumi<= inai+inai;
subr<= inar-inbr;
subi<= inai-inbi;
//multr_part1= subr*twiddle_r;
//multr_part2= subi*twiddle_i;
//multi_part1= subr*twiddle_i;
//multi_part2= subi*twiddle_r;

end
*/



//using lib adder
DW01_addsub #(36) i4(.A(multr_part1[63:28]),.B(multr_part2[63:28]),.CI(1'b0),.ADD_SUB(1'b1),.SUM(outbr),.CO());
DW01_addsub #(36) i5(.A(multi_part1[63:28]),.B(multi_part2[63:28]),.CI(1'b0),.ADD_SUB(1'b0),.SUM(outbi),.CO());

always @(posedge (clk) or posedge(rst)) begin
	if(rst) begin
		outar_d<=0;
		outai_d<=0;
	end else begin 
		outar_d<= sumr;
		outai_d<= sumi;
	end
end
assign outar = /*(inar[27]===1 && inbr[27]===1)?{1'b1,sumr[26:0]}:*/outar_d;
assign outai = /*(inai[27]===1 && inbi[27]===1)?{1'b1,sumi[26:0]}:*/outai_d; 
//assign outbr = multr_part1[64:28]-multr_part2[64:28]; 
//assign outbi = multi_part1[64:28]+multi_part2[64:28]; 
/*
always @(posedge(clk)) begin
sumr <=({multr_part1[55],multr_part1[54:24]}-{multr_part2[55],multr_part2[54:24]});
sumi <=({multi_part1[55],multi_part1[54:24]}+{multi_part2[55],multi_part2[54:24]});
subr <=({multr_part1[55],multr_part1[54:24]}-{multr_part2[55],multr_part2[54:24]});
subi <=({multi_part1[55],multi_part1[54:24]}+{multi_part2[55],multi_part2[54:24]});
end
assign outar = (inar[35] ==1 && sumr[27]==1) ?({1'b1,inar+sumr}):inar+sumr; 
assign outai = (inar[35] ==1 && sumi[27]==1) ?({1'b1,inai+sumi}):inai+sumi;  
assign outbr = inar-subr; 
assign outbi = inai-subi; 
*/
endmodule
